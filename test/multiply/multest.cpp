#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"
#include "myembeded/multiply/mul.h"

TEST_GROUP(MulTest)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};


TEST(MulTest, MulTest1)
{
    int result = multiply(8, 8);
    CHECK_EQUAL(64, result);
}
