#include <stdio.h>
#include "add_sub.h"
#include "../divide/div.h"
#include "../multiply/mul.h"

int main()
{
	printf("\nEXECUTE add(22,2) is %d", add(22, 2));
	printf("\nEXECUTE sub(22,2) is %d", sub(22, 2));
	printf("\nEXECUTE div(22,2) is %d", divide(22, 2));
	printf("\nEXECUTE mul(22,3) is %d", multiply(22, 2));
}