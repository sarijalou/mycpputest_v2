#ifndef DIV_
#define DIV_

#ifdef __cplusplus
extern "C" {
#endif
	int divide(int a, int b);
#ifdef __cplusplus
}
#endif
#endif